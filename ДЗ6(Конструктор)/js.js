// ЗАВДАННЯ 1


/*class Human {

    constructor (age) {
        this.age = age
    }
}

const ivan = new Human (25);
const pizza = new Human (1)
const pogba = new Human (30)
const hize = new Human (5)
const ages = []

ages.push(pizza,ivan,pogba,hize)

ages.sort(function(a,b){
    return a.age - b.age
})

console.log(ages)*/




// ЗАВДАННЯ 2


class Human {
    constructor (name,age){
        this.name = name
        this.age = age
    }
    sayAge(){
        console.log('hi, i am ' + this.age + ' old')
    }
    static createGuest(){
        return new Human('Гость', 'нету информации')
    }
}

const user = Human.createGuest();
alert(user.name)

const qwerty = new Human ('qwerty', 5);
qwerty.sayAge()

